const express = require('express');
const cors = require('cors');
const multer = require('multer');
const upload = multer({ dest: 'profile-pictures/' });
const app = express();
const port = 9000;

app.use(cors());

function profileHandler(request, response) {
    console.log(request.body.name);
    console.log(request.file);
    response.status(200).end();
}

app.put('/profile', upload.single('avatar'), profileHandler);
app.listen(port);